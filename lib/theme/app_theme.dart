import 'package:flutter/material.dart';
import 'package:task_repos/utils/app_colors.dart';

class AppTheme {
  AppTheme._();

  static ThemeData appTheme = ThemeData(
    fontFamily: 'Raleway',
    appBarTheme: const AppBarTheme(
      titleTextStyle: TextStyle(
        color: Colors.black,
        fontFamily: 'Raleway',
        fontWeight: FontWeight.w600,
        fontSize: 16,
      ),
    ),
    textTheme: const TextTheme(
      bodyMedium: TextStyle(
        fontFamily: 'Raleway',
        color: AppColors.gray75Color,
        fontSize: 14,
      ),
    ),
    useMaterial3: true,
  );
}
