import 'package:flutter/foundation.dart';
import 'package:task_repos/feature/models/search_result.dart';
import 'package:task_repos/feature/service/search_service.dart';

class SearchProvider extends ChangeNotifier {
  List<SearchResult> searchResult = [];
  List<SearchResult> filteredSearchResult = [];
  SearchService searchService;

  SearchProvider(this.searchService);

  Future<List<SearchResult>> getAllSearchResults() async {
    final localDb = await searchService.getSearchResult();
    searchResult = [];
    for (var res in localDb) {
      searchResult.add(res);
    }
    notifyListeners();
    return searchResult;
  }

  Future<void> searchResultToLocalDb(SearchResult query) async {
    final localDb = await searchService.getSearchResult();
    final existData = localDb
        .where((element) =>
            element.title.toLowerCase() == query.title.toLowerCase())
        .toList();
    if (existData.isEmpty) {
      searchResult.add(query);
      searchService.addSearchResult(query);
    }
    notifyListeners();
  }

  Future<void> updateIsFavorite(SearchResult query) async {
    await searchService.updateIsFavorite(query);
    searchResult.map((elem) {
      if (elem.id == query.id) {
        elem.isFavorite = query.isFavorite;
      }
    }).toList();

    notifyListeners();
  }

  Future<void> getFilteredSearchResults(SearchResult query) async {
    searchResult.removeWhere((element) => !element.title.contains(query.title));
    filteredSearchResult = searchResult;
    notifyListeners();
  }
}
