const String searchHistory = 'Search History';
const String whatWeFound = 'What we have found';

//AppBar titles
const String gitReposListTitle = 'Github repos list';
const String favoriteListTitle = 'Favorite repos list';

//Nothing find
const String nothingFind =
    'Nothing was find for your search.\n Please check the spelling';
const String noFavorites =
    'You have no favorites.\n Click on star while searching to add first favorite';
const String emptyHistory =
    'You have empty history.\n Click on search to start journey!';

//Hive names
const String cachedsearchResultList = 'CACHED_searchResult_LIST';

//Loading screen
const String searchApp = 'Search App';
