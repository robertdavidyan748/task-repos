import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color primaryColor = Color(0xff1463F5);
  static const Color gray75Color = Color(0xffBFBFBF);
  static const Color gray95Color = Color(0xffF2F2F2);
  static const Color textFieldBackgroundColor = Color(0xffE5EDFF);

}
