abstract class AppImages {
  static const spinnerIcon = 'assets/images/spinner.svg';
  static const starIcon = 'assets/images/star.svg';
  static const searchIcon = 'assets/images/search.svg';
}
