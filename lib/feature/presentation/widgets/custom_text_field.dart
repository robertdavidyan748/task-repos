import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:task_repos/utils/app_colors.dart';
import 'package:task_repos/utils/app_images.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final void Function() onTap;
  final Function(String)? onChanged;

  const CustomTextField({
    required this.focusNode,
    required this.controller,
    required this.onTap,
    this.onChanged,
    super.key,
  });

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _showSuffixIcon = false;
  bool _isFocused = false;

  @override
  void initState() {
    widget.focusNode.addListener(_onFocusChange);
    super.initState();
  }

  @override
  void dispose() {
    widget.focusNode.removeListener(_onFocusChange);
    super.dispose();
  }

  void _updateSuffixIconVisibility() {
    setState(() {
      _showSuffixIcon = widget.controller.text.isNotEmpty;
    });
  }

  void _onFocusChange() {
    setState(() {
      _isFocused = widget.focusNode.hasFocus;
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      focusNode: widget.focusNode,
      onChanged: (value) {
        widget.onChanged!(value);
        _updateSuffixIconVisibility();
      },
      controller: widget.controller,
      decoration: InputDecoration(
        hintText: 'Search',
        hintStyle: const TextStyle(
          color: AppColors.gray75Color,
          fontSize: 14,
          fontFamily: 'Raleway',
        ),
        fillColor: _isFocused
            ? AppColors.textFieldBackgroundColor
            : AppColors.gray95Color,
        filled: true,
        enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: BorderRadius.all(Radius.circular(30))),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            borderSide: BorderSide(color: AppColors.primaryColor, width: 2)),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        prefixIcon: Padding(
          padding: const EdgeInsets.all(16.0),
          child: InkWell(
            onTap: widget.onTap,
            child: SvgPicture.asset(
              AppImages.searchIcon,
            ),
          ),
        ),
        suffixIcon: widget.controller.text.isNotEmpty
            ? GestureDetector(
                child: const Icon(
                  Icons.cancel_outlined,
                  color: AppColors.primaryColor,
                ),
                onTap: () {
                  _updateSuffixIconVisibility();
                  setState(() {
                    widget.controller.text = '';
                  });
                },
              )
            : null,
      ),
    );
  }
}
