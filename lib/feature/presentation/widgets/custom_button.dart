import 'package:flutter/material.dart';
import 'package:task_repos/utils/app_colors.dart';

class CustomButton extends StatelessWidget {
  final void Function()? onTap;
  final bool isStar;
  const CustomButton({required this.onTap, this.isStar = true, super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      color: Colors.white,
      style: ButtonStyle(
          backgroundColor:
              const MaterialStatePropertyAll(AppColors.primaryColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ))),
      onPressed: onTap,
      icon: isStar
          ? const Icon(Icons.star_rounded)
          : const Icon(Icons.arrow_back_ios_new),
    );
  }
}
