import 'package:flutter/material.dart';
import 'package:task_repos/feature/models/search_result.dart';
import 'package:task_repos/utils/app_colors.dart';

// ignore: must_be_immutable
class CustomListTile extends StatefulWidget {
  SearchResult searchResult;
  void Function()? onTap;
  CustomListTile({
    super.key,
    required this.searchResult,
    this.onTap,
  });

  @override
  State<CustomListTile> createState() => _CustomListTileState();
}

class _CustomListTileState extends State<CustomListTile> {
  bool isFavorite = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: ListTile(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: Text(widget.searchResult.title),
        tileColor: AppColors.gray95Color,
        trailing: GestureDetector(
          onTap: widget.onTap,
          child: Icon(
            size: 44,
            Icons.star_rounded,
            color: widget.searchResult.isFavorite
                ? AppColors.primaryColor
                : AppColors.gray75Color,
          ),
        ),
      ),
    );
  }
}
