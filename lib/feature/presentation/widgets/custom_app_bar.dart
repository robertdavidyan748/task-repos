import 'package:flutter/material.dart';
import 'package:task_repos/feature/presentation/screens/favorite_screen.dart';
import 'package:task_repos/feature/presentation/widgets/custom_button.dart';
import 'package:task_repos/utils/app_colors.dart';

AppBar customAppBar({
  required BuildContext context,
  required String title,
  bool isFavoriteScreen = true,
}) {
  return AppBar(
    leading: isFavoriteScreen
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomButton(
              onTap: () => Navigator.of(context).pop(),
              isStar: false,
            ),
          )
        : null,
    bottom: const PreferredSize(
      preferredSize: Size.fromHeight(1.0),
      child: Divider(
        color: AppColors.gray95Color,
        height: 1.0,
      ),
    ),
    title: Text(
      title,
    ),
    centerTitle: true,
    actions: [
      if (!isFavoriteScreen)
        CustomButton(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => const FavoriteScreen(),
            ));
          },
        ),
    ],
  );
}
