import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task_repos/feature/models/search_result.dart';
import 'package:task_repos/feature/presentation/widgets/custom_app_bar.dart';
import 'package:task_repos/feature/presentation/widgets/custom_list_tile.dart';
import 'package:task_repos/feature/presentation/widgets/custom_text_field.dart';
import 'package:task_repos/provider/search_provider.dart';
import 'package:task_repos/utils/app_colors.dart';
import 'package:task_repos/utils/constant.dart';
import 'package:http/http.dart' as http;

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  void initState() {
    context.read<SearchProvider>().getAllSearchResults();
    super.initState();
  }
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  bool _showFetchedData = false;
  bool _isFoundText = false;
  List<SearchResult> _repositories = [];

  Future<void> _searchRepositories(String query) async {
    final response = await http.get(
      Uri.parse(
          'https://api.github.com/search/repositories?q=$query+in:name&per_page=15'),
      headers: {
        'Accept': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      setState(() {
        final List<dynamic> items = jsonDecode(response.body)['items'];
        _repositories = items
            .map((item) => SearchResult(id: item['id'], title: item['name']))
            .toList();
      });
    } else {
      throw Exception('Failed to load repositories');
    }
  }

  @override
  void dispose() {
    controller.dispose();
    focusNode.dispose();
    super.dispose();
  }

  void _updateIsFavorite(List<SearchResult> getData, int index) {
    setState(() {
      getData[index].isFavorite = !getData[index].isFavorite;
    });
  }

  @override
  Widget build(BuildContext context) {
    final searchProvider = context.read<SearchProvider>();
    SearchResult param = SearchResult(
      id: DateTime.now().microsecondsSinceEpoch,
      title: controller.text.trim(),
    );

    return Scaffold(
      appBar: customAppBar(
        context: context,
        title: gitReposListTitle,
        isFavoriteScreen: false,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextField(
              onChanged: (value) {
                if (value == '') _showFetchedData = false;
                setState(() {});
              },
              onTap: () {
                if (controller.text != '') {
                  setState(() {
                    searchProvider.searchResultToLocalDb(param);
                    searchProvider.getFilteredSearchResults(param);
                    _searchRepositories(param.title);
                    _showFetchedData = true;
                    _isFoundText = true;
                  });
                }
              },
              controller: controller,
              focusNode: focusNode,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Text(
                _isFoundText ? whatWeFound : searchHistory,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: AppColors.primaryColor,
                ),
              ),
            ),
            Flexible(
              child: FutureBuilder(
                future: context.read<SearchProvider>().getAllSearchResults(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(child: CupertinoActivityIndicator());
                  }
                  if (snapshot.data!.isNotEmpty) {
                    final List<SearchResult> history = snapshot.data!;
                    final filteredHistory = history
                        .where((element) => element.title
                            .toLowerCase()
                            .startsWith(controller.text.toLowerCase()))
                        .toList();
                    final filteredRepositories = _repositories
                        .where((element) => element.title
                            .toLowerCase()
                            .startsWith(controller.text.toLowerCase()))
                        .toList();
                    return _showFetchedData
                        ? _buildListView(
                            (index) => _updateIsFavorite(
                                  filteredRepositories,
                                  index,
                                ),
                            filteredRepositories,
                            searchProvider)
                        : _buildListView(
                            (index) => _updateIsFavorite(
                              filteredHistory,
                              index,
                            ),
                            filteredHistory,
                            searchProvider,
                          );
                  } else if (snapshot.data!.isEmpty) {
                    return const Center(
                      child: Text(
                        emptyHistory,
                        textAlign: TextAlign.center,
                      ),
                    );
                  }
                  return const SizedBox();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListView(
    void Function(int) updateIsFavorite,
    List<SearchResult> getData,
    SearchProvider searchProvider,
  ) {
    final reversedList = getData.reversed.toList();
    final last15Items = reversedList.take(15).toList();

    if (getData.isEmpty) {
      return const Center(
        child: Text(
          nothingFind,
          textAlign: TextAlign.center,
        ),
      );
    }

    return ListView.builder(
      shrinkWrap: true,
      itemBuilder: (context, index) {
        if (getData.isNotEmpty) {
          final reversedIndex = reversedList.length - 1 - index;

          return CustomListTile(
            onTap: () => {
              searchProvider.updateIsFavorite(reversedList[index]),
              updateIsFavorite(reversedIndex)
            },
            searchResult: SearchResult(
              id: reversedList[index].id,
              title: reversedList[index].title,
              isFavorite: reversedList[index].isFavorite,
            ),
          );
        }
        return const SizedBox();
      },
      itemCount: last15Items.length,
    );
  }
}
