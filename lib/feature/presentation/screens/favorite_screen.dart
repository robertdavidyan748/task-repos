import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task_repos/feature/models/search_result.dart';
import 'package:task_repos/feature/presentation/widgets/custom_app_bar.dart';
import 'package:task_repos/feature/presentation/widgets/custom_list_tile.dart';
import 'package:task_repos/provider/search_provider.dart';
import 'package:task_repos/utils/constant.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({super.key});

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  @override
  Widget build(BuildContext context) {
    final searchProvider = context.read<SearchProvider>();
    final favoritesSearchResult = searchProvider.searchResult
        .where((element) => element.isFavorite == true)
        .toList();
    return Scaffold(
      appBar: customAppBar(context: context, title: favoriteListTitle),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        child: favoritesSearchResult.isNotEmpty
            ? ListView.builder(
                itemCount: favoritesSearchResult.length,
                itemBuilder: (context, index) {
                  return CustomListTile(
                    searchResult: SearchResult(
                      id: favoritesSearchResult[index].id,
                      title: favoritesSearchResult[index].title,
                      isFavorite: favoritesSearchResult[index].isFavorite,
                    ),
                  );
                },
              )
            : const Center(
                child: Text(
                  noFavorites,
                  textAlign: TextAlign.center,
                ),
              ),
      ),
    );
  }
}
