import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:task_repos/utils/app_colors.dart';
import 'package:task_repos/utils/constant.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              searchApp,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          CupertinoActivityIndicator(),
        ],
      )),
    );
  }
}
