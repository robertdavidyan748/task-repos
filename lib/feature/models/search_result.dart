// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'search_result.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class SearchResult {
  @HiveField(0)
  final int id;

  @HiveField(1)
  final String title;

  @HiveField(2, defaultValue: false)
  bool isFavorite;

  SearchResult({
    required this.id,
    required this.title,
    this.isFavorite = false,
  });

  factory SearchResult.fromJson(Map<String, dynamic> json) =>
      _$SearchResultFromJson(json);

  Map<String, dynamic> toJson() => _$SearchResultToJson(this);

  @override
  bool operator ==(covariant SearchResult other) {
    if (identical(this, other)) return true;

    return 
        other.title == title &&
        other.isFavorite == isFavorite;
  }

  @override
  int get hashCode => id.hashCode ^ title.hashCode ^ isFavorite.hashCode;
}
