import 'package:hive/hive.dart';
import 'package:task_repos/feature/models/search_result.dart';
import 'package:task_repos/utils/constant.dart';

class SearchService {
  final String _boxName = cachedsearchResultList;

  Future<Box<SearchResult>> get box async =>
      await Hive.openBox<SearchResult>(_boxName);

  Future<void> addSearchResult(SearchResult searchResult) async {
    var localData = await box;
    var listOfData = localData.values.toList();

    var foundItems = listOfData.where((element) =>
        element.title.toLowerCase() == searchResult.title.toLowerCase());
    if (foundItems.isEmpty) {
      localData.add(searchResult);
    }
  }

  Future<List<SearchResult>> getSearchResult() async {
    var localData = await box;
    var dataToList = localData.values.toList();

    return dataToList;
  }

  Future<void> updateIsFavorite(SearchResult searchResult) async {
    var localData = await box;
    final localDatatoMap = localData.toMap();
    int itemKey = -1;

    localDatatoMap.forEach((key, value) {
      if (value.id == searchResult.id) {
        itemKey = key;
      }
    });

    await localData.put(
        itemKey > -1 ? itemKey : localDatatoMap.length, searchResult);
  }
}
