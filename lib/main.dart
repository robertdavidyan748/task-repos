import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:task_repos/feature/models/search_result.dart';
import 'package:task_repos/feature/presentation/screens/loading_screen.dart';
import 'package:task_repos/feature/presentation/screens/search_screen.dart';
import 'package:task_repos/feature/service/search_service.dart';
import 'package:task_repos/provider/search_provider.dart';
import 'package:task_repos/theme/app_theme.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final dir = await getApplicationDocumentsDirectory();
  Hive.init(dir.path);
  Hive.registerAdapter(SearchResultAdapter());
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  SearchService searchService = SearchService();

  Future<void> _loadData() async {
    await Future.delayed(const Duration(seconds: 3));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => SearchProvider(searchService),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: AppTheme.appTheme,
        home: FutureBuilder(
          future: _loadData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
             return const LoadingScreen();
            }
            return const SearchScreen();
          }, 
        ),
      ),
    );
  }
}
